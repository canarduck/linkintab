#!/usr/bin/env python3
"""
Compression en zip de l’extension
"""
from configparser import ConfigParser
import shutil

config = ConfigParser()
config.read('setup.cfg')

version = config['bumpversion']['current_version']

archive = 'dist/linkintab-{version}'.format(version=version)

shutil.make_archive(archive, 'zip', 'linkintab')

print(archive + ' généré')
