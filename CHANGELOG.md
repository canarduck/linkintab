# Journal des modifications

Ce fichier est généré automatiquement après chaque modification du dépôt.
Les numéros indiqués correspondent aux [versions taggées](https://gitlab.com/canarduck/linkintab/tags).

## Version actuelle

### ★ Nouveautés

* Script génération package extension.

### ⚔ Correctifs

* Application id pour accès storage.
* Strict_min_version.

## v0.1.3 (2018-04-12)

### ✍ Changements

* Refonte structure du projet.

### ✓ Autres

* Prise en compte des settings, extraction domaine à faire.
* Settings fonctionnels.
* Initial commit.

