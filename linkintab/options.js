function saveOptions(e) {
    e.preventDefault();
    browser.storage.local.set({
        level: document.querySelector("input[name=level]:checked").value,
        method: document.querySelector("input[name=method]:checked").value
    });
}

function restoreOptions() {

    function setLevelChoice(result) {
        var level = result.level || "0";
        document.querySelector("#level_" + level).checked = true;
    }
    function setMethodChoice(result) {
        var method = result.method || "host";
        document.querySelector("#method_" + method).checked = true;
    }

    function onError(error) {
        console.log(`Error: ${error}`);
    }

    var getting = browser.storage.local.get("level");
    getting.then(setLevelChoice, onError);
    var getting = browser.storage.local.get("method");
    getting.then(setMethodChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);