function onError(error) {
    console.log(`Error: ${error}`);
}

const TARGET = "slop"
const DEFAULT_LEVEL = "0"
const DEFAULT_METHOD = "domain"

function onGot(result) {
    var level = result.level || DEFAULT_LEVEL;
    var method = result.method || DEFAULT_METHOD;

    if(level == "0"){ // rien n’est à linkintabiser
        return;
    }

    current_url = new URL(window.location);
    
    var links = document.getElementsByTagName('a');
    for (var i=0; i<links.length; i++) {
        if(level == "2"){ // tout est à linkintabiser
            links[i].target = TARGET;
        } else {
            let link_url = new URL(links[i].href)
            if(method == 'hostname' && link_url.hostname != current_url.hostname){
                links[i].target = TARGET;
            }
            // TODO: domain
        }
    }
}

var getting = browser.storage.local.get(["level", "method"]);
getting.then(onGot, onError);